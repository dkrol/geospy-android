using System.Text;

namespace GeoSpyMobile.Models
{
    public class HttpResponse
    {
        public byte[] Response { get; set; }
        public string ResponsePost { get; set; }
        public bool Status { get; set; }

        public string ResponseJson
        {
            get
            {
                if (string.IsNullOrEmpty(this.ResponsePost))
                {
                    string result = string.Empty;
                    if (Response != null)
                    {
                        result = Encoding.UTF8.GetString(Response);
                    }
                    return result;
                }
                else
                {
                    return this.ResponsePost;
                }
            }
        }
    }
}