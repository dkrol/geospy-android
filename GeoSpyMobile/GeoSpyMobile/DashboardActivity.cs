using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Content.PM;
using Plugin.Permissions;
using GeoSpyMobile.Helpers;
using GeoSpyMobile.Config;
using GeoSpyMobile.Services;
using Plugin.Geolocator.Abstractions;
using GeoSpyMobile.Models;
using Newtonsoft.Json;

namespace GeoSpyMobile
{
    [Activity(Label = "Dashboard", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class DashboardActivity : Activity
    {
        public static Activity CurrentActivity;
        private Button _btnLogout;
        private Intent _locationService;
        private TextView _txtUserLogin;
        private TextView _txtDeviceName;
        private TextView _txtDeviceDescription;
        private TextView _txtDeviceImei;
        private static TextView _txtLatitude;
        private static TextView _txtLongitude;
        private static TextView _txtSpeed;
        private static TextView _txtDistance;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.DashboardLayout);
            CurrentActivity = this;

            InitView();
            InitService();
            InitData();
        }

        private void InitView()
        {
            _txtUserLogin = FindViewById<TextView>(Resource.Id.txtUserLogin);
            _txtDeviceName = FindViewById<TextView>(Resource.Id.txtDeviceName);
            _txtDeviceDescription = FindViewById<TextView>(Resource.Id.txtDeviceDescription);
            _txtDeviceImei = FindViewById<TextView>(Resource.Id.txtDeviceImei);
            _txtLatitude = FindViewById<TextView>(Resource.Id.txtLatitude);
            _txtLongitude = FindViewById<TextView>(Resource.Id.txtLongitude);
            _txtSpeed = FindViewById<TextView>(Resource.Id.txtSpeed);
            _txtDistance = FindViewById<TextView>(Resource.Id.txtDistance);
            _btnLogout = FindViewById<Button>(Resource.Id.btnLogout);
            _btnLogout.Click += BtnLogout_Click;
        }

        private void InitService()
        {
            _locationService = new Intent(this, typeof(LocationService));
            StartService(_locationService);
        }

        private void InitData()
        {
            string deviceJson = SharedPref.GetString(AppConfig.DeviceConfig, AppConfig.Device);
            string tokenJson = SharedPref.GetString(AppConfig.UserConfig, AppConfig.Token);
            Device device = JsonConvert.DeserializeObject<Device>(deviceJson);
            Token token = JsonConvert.DeserializeObject<Token>(tokenJson);

            _txtUserLogin.Text = token.userName;
            _txtDeviceName.Text = device.Name;
            _txtDeviceDescription.Text = string.IsNullOrEmpty(device.Description) ? "-" : device.Description;
            _txtDeviceImei.Text = device.Imei;
        }

        private void BtnLogout_Click(object sender, EventArgs e)
        {
            StopService(_locationService);
            SharedPref.Clear(AppConfig.UserConfig);
            Intent intent = new Intent(this, typeof(SplashActivity));
            this.StartActivity(intent);
            this.Finish();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public static void UpdateView(Location location, string distance)
        {
            _txtLatitude.Text = location.Latitude;
            _txtLongitude.Text = location.Longitude;
            _txtSpeed.Text = location.Speed.ToString();
            _txtDistance.Text = distance;
        }
    }


    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { "geospy.location.changed" })]
    public class LocationReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            if (intent.Action == "geospy.location.changed")
            { 
                Position pos = new Position();
                Location location = new Location()
                {
                    Latitude = intent.GetDoubleExtra("Latitude", 0).ToString(),
                    Longitude = intent.GetDoubleExtra("Longitude", 0).ToString(),
                    Speed = Math.Round(intent.GetDoubleExtra("Speed", 0))
                };

                string distance = Math.Round(intent.GetDoubleExtra("Distance", 0)).ToString();
                DashboardActivity.UpdateView(location, distance);
            }
        }
    }
}