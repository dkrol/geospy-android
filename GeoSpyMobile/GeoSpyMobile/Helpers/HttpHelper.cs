using System;
using System.Net;

namespace GeoSpyMobile.Helpers
{
    public class HttpHelper
    {
        private readonly WebClient webClient;
        private readonly Uri url;

        public event DownloadDataCompletedEventHandler DownloadDataCompleted
        {
            add
            {
                webClient.DownloadDataCompleted += value;
            }
            remove
            {
                webClient.DownloadDataCompleted -= value;
            }
        }

        public HttpHelper(string url)
        {
            this.webClient = new WebClient();
            this.url = new Uri(url);
        }

        public void DownloadDataAsync()
        {
            webClient.DownloadDataAsync(url);
        }

        public void DownloadDataAsync(DownloadDataCompletedEventHandler downloadDataCompleted)
        {
            DownloadDataAsync();
            webClient.DownloadDataCompleted += downloadDataCompleted;
        }
    }
}