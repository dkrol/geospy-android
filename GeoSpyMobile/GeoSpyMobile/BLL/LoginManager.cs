using GeoSpyMobile.Models;
using GeoSpyMobile.Helpers;
using GeoSpyMobile.Config;

namespace GeoSpyMobile.BLL
{
    public class LoginManager
    {
        public bool Signin(IUserLogin user)
        {
            HttpResponse response = Http.Signin(AppConfig.SigninUrl, user);
            if (response.Status)
            {
                SharedPref.PutString(AppConfig.UserConfig, AppConfig.Token, response.ResponseJson);
            }
            return response.Status;
        }
    }
}