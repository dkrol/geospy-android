namespace GeoSpyMobile.Models
{
    public interface IUserSignup
    {
        string UserName { get; set; }
        string Password { get; set; }
        string ConfirmPassword { get; set; }
    }
}