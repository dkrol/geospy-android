using GeoSpyMobile.Models;
using GeoSpyMobile.Helpers;
using GeoSpyMobile.Config;

namespace GeoSpyMobile.BLL
{
    public class SignupManager
    {
        public bool SignupUser(IUserSignup user)
        {
            HttpResponse response = Http.Post(AppConfig.SignupUrl, user); 
            return response.Status;
        }
    }
}