namespace GeoSpyMobile.Models
{
    public class Location
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double Speed { get; set; }
        public long Timestamp { get; set; }
        public string DeviceId { get; set; }
    }
}