using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using GeoSpyMobile.Models;
using GeoSpyMobile.BLL;

namespace GeoSpyMobile.Services
{
    [Service(Exported = true, Name = "geospy.LocationService")]
    public class LocationService : Service
    {
        private const int MinTime = 1000;
        private const double MinDistance = 1;
        private const double DesiredAccuracy = 5;
        private const int MinDistanceBetweenCoordinates = 50;
        private Device _device;
        private LocationManager _locationManager;
        private DeviceManager _deviceManager;
        private IGeolocator _locator;
        private static Position _lastPosition;

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnCreate()
        {
            base.OnCreate();
            _locationManager = new LocationManager();
            _deviceManager = new DeviceManager();
            _device = _deviceManager.GetDeviceFromSharedPref();
            InitLocation();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            _locator.StopListeningAsync();
        }

        private async void InitLocation()
        {
            try
            {
                _locator = CrossGeolocator.Current;
                _locator.DesiredAccuracy = DesiredAccuracy;
                _locator.AllowsBackgroundUpdates = true;
                _locator.PositionChanged += Locator_PositionChanged;
                _locator.PositionError += Locator_PositionError;
                await _locator.StartListeningAsync(MinTime, MinDistance, true);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "No localization", ToastLength.Short).Show();
                Console.WriteLine(ex.Message);
            }
        }
        
        private Location GetLocationFromPosition(Position position)
        {
            return new Location
            {
                Latitude = position.Latitude.ToString().Replace(',','.'),
                Longitude = position.Longitude.ToString().Replace(',', '.'),
                Timestamp = long.Parse(position.Timestamp.ToLocalTime().ToString("yyyyMMddHHmmss").Replace('.', '/')),
                Speed = position.Speed,
                DeviceId = _device.Id
            };
        }

        private void SendLocation(Location location, double distance)
        {
            _locationManager.AddLocation(location);

            Console.WriteLine("Position Status: {0}", location.Timestamp);
            Console.WriteLine("Position Latitude: {0}", location.Latitude);
            Console.WriteLine("Position Longitude: {0}", location.Longitude);            
        }

        private double DegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180;
        }

        private double DistanceBetweenEarthCoordinatesInMeters(double lat1, double lon1, double lat2, double lon2)
        {
            var earthRadiusKm = 6371;

            var dLat = DegreesToRadians(lat2 - lat1);
            var dLon = DegreesToRadians(lon2 - lon1);

            lat1 = DegreesToRadians(lat1);
            lat2 = DegreesToRadians(lat2);

            var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return (earthRadiusKm * c) * 1000;
        }
        
        private void Locator_PositionChanged(object sender, PositionEventArgs e)
        {
            Position position = e.Position;
            bool sendPosition = _lastPosition == null;
            double distance = 0;

            if (!sendPosition)
            {
                distance = DistanceBetweenEarthCoordinatesInMeters(
                    _lastPosition.Latitude, _lastPosition.Longitude, 
                    position.Latitude, position.Longitude);

                sendPosition = distance > MinDistanceBetweenCoordinates;
                Console.WriteLine("DISTANCE: {0}", distance);
            }

            if (sendPosition)
            {
                Location location = GetLocationFromPosition(position);
                SendLocation(location, distance);
                _lastPosition = position;
            }

            var intent = new Intent("geospy.location.changed");
            intent.PutExtra("Latitude", position.Latitude);
            intent.PutExtra("Longitude", position.Longitude);
            intent.PutExtra("Speed", position.Speed);
            intent.PutExtra("Distance", distance);

            SendBroadcast(intent);
        }
        private void Locator_PositionError(object sender, PositionErrorEventArgs e)
        {
            Console.WriteLine("Locator_PositionError!");
        }
    }
}