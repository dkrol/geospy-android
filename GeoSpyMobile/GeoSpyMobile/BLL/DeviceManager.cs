using System.Collections.Generic;
using Android.App;
using Android.Content;
using GeoSpyMobile.Models;
using Android.Telephony;
using GeoSpyMobile.Helpers;
using GeoSpyMobile.Config;
using Newtonsoft.Json;

namespace GeoSpyMobile.BLL
{
    public class DeviceManager
    {
        private readonly Activity _activity;
        public DeviceManager(Activity activity)
        {
            this._activity = activity;
        }

        public DeviceManager() : this(null) { }

        public void AddDevice(IUserLogin user)
        {
            Device device = GetCurrentDevice();
            device.UserName = user.UserName;
            HttpResponse response = Http.Post(AppConfig.DeviceUrl, device, setToken: true);
            if (response.Status)
            {
                SharedPref.PutString(AppConfig.DeviceConfig, AppConfig.Device, response.ResponseJson); 
            }
        }

        public Device GetCurrentDevice()
        {
            Device device = null;
            if (_activity != null)
            {
                device = new Device();
                TelephonyManager telephonyManager = (TelephonyManager)_activity.ApplicationContext.GetSystemService(Context.TelephonyService);
                device.Imei = telephonyManager.DeviceId;
                device.Name = telephonyManager.MmsUserAgent;
            }
            return device;
        }

        public List<Device> GetDevices()
        {
            HttpResponse response = Http.Get(AppConfig.DeviceUrl, setToken: true);
            List<Device> devices = JsonConvert.DeserializeObject<List<Device>>(response.ResponseJson);
            return devices;
        }

        public Device GetDeviceFromSharedPref()
        {
            string deviceJson = SharedPref.GetString(AppConfig.DeviceConfig, AppConfig.Device);
            Device device = JsonConvert.DeserializeObject<Device>(deviceJson);
            return device;
        }
    }
}