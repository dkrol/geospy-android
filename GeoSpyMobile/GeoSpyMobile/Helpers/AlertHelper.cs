using Android.App;
using Android.Widget;

namespace GeoSpyMobile.Helpers
{
    public class AlertHelper
    {
        public enum AlertType
        {
            Inform,
            Confirm
        };

        public string Title { get; set; }
        public string Message { get; set; }
        private readonly AlertDialog.Builder alertBuilder;
        private readonly Activity activity;
        public AlertHelper(Activity activity)
        {
            this.activity = activity;
            alertBuilder = new AlertDialog.Builder(activity);
        }

        public AlertHelper(Activity activity, string Title, string Message) : this(activity)
        {
            this.Title = Title;
            this.Message = Message;
        }

        public void Show(AlertType type)
        {
            switch(type)
            {
                case AlertType.Inform:
                    SetOkButton();
                    break;
                case AlertType.Confirm:
                    SetYesNoButtons();
                    break;
            }

            alertBuilder.SetTitle(Title);
            alertBuilder.SetMessage(Message);
            Dialog dialog = alertBuilder.Create();
            dialog.Show();
        }

        private void SetYesNoButtons()
        {
            alertBuilder.SetPositiveButton("Yes", (senderAlert, args) =>
            {
                Toast.MakeText(activity, "Success", ToastLength.Short).Show();
            });

            alertBuilder.SetNegativeButton("No", (senderAlert, args) => {});
        }

        private void SetOkButton()
        {
            alertBuilder.SetPositiveButton("Ok", (senderAlert, args) => {});
        }
    }
}