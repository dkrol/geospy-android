using GeoSpyMobile.Models;
using GeoSpyMobile.Config;
using GeoSpyMobile.Helpers;

namespace GeoSpyMobile.BLL
{
    class LocationManager
    {
        public void AddLocation(Location location)
        {
            HttpResponse response = Http.Post(AppConfig.LocationUrl, location, setToken: true);
        }
    }
}