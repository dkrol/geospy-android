namespace GeoSpyMobile.Models
{
    public interface IUserLogin
    {
        string UserName { get; set; }
        string Password { get; set; }
    }
}