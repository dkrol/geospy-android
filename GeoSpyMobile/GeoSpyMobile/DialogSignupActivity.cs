using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using GeoSpyMobile.Models;

namespace GeoSpyMobile
{
    class DialogSignupData : Activity
    {
        public EditText TxtLogin { get; set; }
        public EditText TxtPassword { get; set; }
        public EditText TxtConfirmPassword { get; set; }
        public Button BtnSignup { get; set; }
        public TextView TxtLoginValid { get; set; }
        public TextView TxtPasswordValid { get; set; }
        public TextView TxtConfirmPasswordValid { get; set; }
        public View View { get; set; }

        public DialogSignupData(View view)
        {   
            this.View = view;
            TxtLogin = view.FindViewById<EditText>(Resource.Id.txtLoginDialog);
            TxtPassword = view.FindViewById<EditText>(Resource.Id.txtPasswordDialog);
            TxtConfirmPassword = view.FindViewById<EditText>(Resource.Id.txtConfirmPasswordDialog);
            BtnSignup = view.FindViewById<Button>(Resource.Id.btnSignupDialog);
            TxtLoginValid = view.FindViewById<TextView>(Resource.Id.txtLoginDialogValid);
            TxtPasswordValid = view.FindViewById<TextView>(Resource.Id.txtPasswordDialogValid);
            TxtConfirmPasswordValid = view.FindViewById<TextView>(Resource.Id.txtConfirmPasswordDialogValid);
        }
    }

    class OnSignupEventArgs : EventArgs
    {
        public IUserSignup User { get; set; }

        public OnSignupEventArgs(DialogSignupData data) : base()
        {
            User = new User
            {
                UserName = data.TxtLogin.Text,
                Password = data.TxtPassword.Text,
                ConfirmPassword = data.TxtConfirmPassword.Text
            };
        }
    } 

    class DialogSignupActivity : DialogFragment
    {
        public event EventHandler<OnSignupEventArgs> OnSignupComplete;
        private DialogSignupData _data;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.DialogSingupLayout, container, false);
            _data = new DialogSignupData(view);
            _data.BtnSignup.Click += BtnSignup_Click; 
            return view;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
        }

        private void BtnSignup_Click(object sender, EventArgs e)
        {
            bool isValid = ValidationData(_data);
            if (isValid)
            {
                OnSignupComplete?.Invoke(this, new OnSignupEventArgs(_data));
                this.Dismiss();
            }
        }

        private bool ValidationData(DialogSignupData data)
        {
            bool result = true;
            if (string.IsNullOrEmpty(data.TxtLogin.Text))
            {
                data.TxtLoginValid.Text = Resources.GetString(Resource.String.RequiredField);
                result = false;
            }
            else
            {
                data.TxtLoginValid.Text = string.Empty;
            }

            if (string.IsNullOrEmpty(data.TxtPassword.Text))
            {
                data.TxtPasswordValid.Text = Resources.GetString(Resource.String.RequiredField);
                result = false;
            }
            else
            {
                data.TxtPasswordValid.Text = string.Empty;
            }

            if (string.IsNullOrEmpty(data.TxtConfirmPassword.Text))
            {
                data.TxtConfirmPasswordValid.Text = Resources.GetString(Resource.String.RequiredField);
                result = false;
            }
            else
            {
                data.TxtConfirmPasswordValid.Text = string.Empty;
            }

            return result;
        }
    }
}