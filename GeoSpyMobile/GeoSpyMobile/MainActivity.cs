﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using System.Threading;
using GeoSpyMobile.Models;
using GeoSpyMobile.BLL;
using Android.Content;

namespace GeoSpyMobile
{
    [Activity(Label = "GeoSpy", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        private DeviceManager _deviceManager;
        private LoginManager _loginManager;
        private SignupManager _signupManager;
        private Button _btnSignin;
        private Button _btnSignup;
        private ProgressBar _progressBar;
        private TextView _txtSigninValid;
        private EditText _txtUserName;
        private EditText _txtPassword;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            _deviceManager = new DeviceManager(this);
            _loginManager = new LoginManager();
            _signupManager = new SignupManager();

            InitViewReferences();
            InitEvents();
        }

        private void InitViewReferences()
        {
            _progressBar = FindViewById<ProgressBar>(Resource.Id.progressBar);
            _btnSignin = FindViewById<Button>(Resource.Id.btnSignin);
            _btnSignup = FindViewById<Button>(Resource.Id.btnSignup);
            _txtSigninValid = FindViewById<TextView>(Resource.Id.txtSigninValid);
            _txtUserName = FindViewById<EditText>(Resource.Id.txtUserName);
            _txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);
        }

        private void InitEvents()
        {
            _btnSignup.Click += BtnSignup_Click;
            _btnSignin.Click += BtnSignin_Click;
        }

        private void BtnSignin_Click(object sender, EventArgs e)
        {
            IUserLogin user = new User();

            user.UserName = _txtUserName.Text;
            user.Password = _txtPassword.Text;

            bool result = _loginManager.Signin((IUserLogin) user);
            if (result)
            {
                _deviceManager.AddDevice(user);
                Intent intent = new Intent(this, typeof(SplashActivity));
                this.StartActivity(intent);
                this.Finish();
            }
            else
            {
                _txtSigninValid.Text = Resources.GetString(Resource.String.IncorrectLoginOrPassword);
            }
        }

        private void BtnSignup_Click(object sender, System.EventArgs e)
        {
            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            DialogSignupActivity dialogSingup = new DialogSignupActivity();
            dialogSingup.Show(transaction, "DialogSignupLayout");
            dialogSingup.OnSignupComplete += DialogSingup_onSignupComplete;
        }

        private void DialogSingup_onSignupComplete(object sender, OnSignupEventArgs e)
        {
            _progressBar.Visibility = Android.Views.ViewStates.Visible;
            Thread thread = new Thread(ActionRequest);
            thread.Start();
            IUserSignup user = e.User;
            bool result = _signupManager.SignupUser(user);

            string msg = result
                ? Resources.GetString(Resource.String.RegistrationSuccess)
                : Resources.GetString(Resource.String.RegistrationError);

            Toast.MakeText(this, msg, ToastLength.Short).Show();
        }

        private void ActionRequest()
        {
            Thread.Sleep(3000);
            RunOnUiThread(() => { _progressBar.Visibility = Android.Views.ViewStates.Invisible; });
        }
    }
}

