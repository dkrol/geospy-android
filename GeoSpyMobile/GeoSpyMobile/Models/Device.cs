namespace GeoSpyMobile.Models
{
    public class Device
    {
        public string Id { get; set; }
        public string Imei { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
    }
}