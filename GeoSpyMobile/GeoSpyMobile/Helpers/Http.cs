using System;
using System.Linq;
using System.Collections.Specialized;
using System.Net;
using System.Threading.Tasks;
using GeoSpyMobile.Models;
using GeoSpyMobile.Config;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace GeoSpyMobile.Helpers
{
    public static class Http
    {
        public static HttpResponse Get(string uri, bool setToken = false)
        {
            HttpResponse httpResponse = new HttpResponse();
            using (WebClient client = new WebClient())
            {
                try
                {
                    if (setToken)
                    {
                        Token token = GetToken();
                        string bearerToken = "Bearer " + token.access_token;
                        client.Headers.Add("Authorization", bearerToken);
                    }
                    httpResponse.Response = client.DownloadData(uri);
                    httpResponse.Status = true;
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    httpResponse.Status = false;
                }
                
            }
            return httpResponse;
        }

        public static HttpResponse Post(string uri, object obj, bool setToken = false)
        {
            HttpResponse httpResponse = new HttpResponse();
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            string data = JsonConvert.SerializeObject(obj, settings);

            using (WebClient client = new WebClient())
            {
                try
                {
                    if (setToken)
                    {
                        Token token = GetToken();
                        client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token.access_token;
                    }

                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    httpResponse.ResponsePost = client.UploadString(uri, data);
                    httpResponse.Status = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    httpResponse.Status = false;
                }
            }
            return httpResponse;
        }

        public static HttpResponse Signin(string uri, object obj)
        {
            HttpResponse httpResponse = new HttpResponse();
            NameValueCollection data = GetNameValueCollection(obj);
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    httpResponse.Response = client.UploadValues(uri, data);
                    httpResponse.Status = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    httpResponse.Status = false;
                }
            }
            return httpResponse;
        }

        private static Token GetToken()
        {
            string tokenJson = SharedPref.GetString(AppConfig.UserConfig, AppConfig.Token);
            Token token = JsonConvert.DeserializeObject<Token>(tokenJson);
            return token;
        }

        private static NameValueCollection GetNameValueCollection(object obj)
        {
            NameValueCollection formFields = new NameValueCollection();
            var formFieldsProp = obj.GetType().GetProperties().ToList();
            foreach (var prop in formFieldsProp)
            {
                var value = prop.GetValue(obj, null);
                if (value != null)
                {
                    formFields.Add(prop.Name, value.ToString());
                }
            }
            return formFields;
        }
    }
}