namespace GeoSpyMobile.Config
{
    public class AppConfig
    {
        //public const string ApiUrl = "http://192.168.137.1:5001/api";
        //public const string ApiUrl = "http://localhost:5001/api";
        public const string ApiUrl = "http://dkrol.ddns.net:5002/api";

        public const string SigninUrl = ApiUrl + "/jwt";
        public const string UsersUrl = ApiUrl + "/users";

        public const string SignupUrl = ApiUrl + "/users";
        //public const string SIGNUP_URL = API_URL + "/api/Account/Register";

        public const string DeviceUrl = ApiUrl + "/devices";
        public const string LocationUrl = ApiUrl + "/locations";

        public const string UserConfig = "USER_CONFIG";
        public const string Token = "TOKEN";

        public const string DeviceConfig = "DEVICE_CONFIG";
        public const string Device = "DEVICE_INFO";
    }
}