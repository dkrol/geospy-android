namespace GeoSpyMobile.Models
{
    public class User : IUserSignup, IUserLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}